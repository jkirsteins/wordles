![GitLab logo](gfx/wordle/wordle.svg){.wordle}

A [word cloud](https://en.wikipedia.org/wiki/Tag_cloud) (a.k.a. *wordle*) made with [Jason Davies](https://www.jasondavies.com/)’s open source [generator](https://www.jasondavies.com/wordcloud/).
